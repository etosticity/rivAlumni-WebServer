// Required Packages
const path = require('path');
const logger = require('morgan');
const express = require('express');
const createError = require('http-errors');
const cookieParser = require('cookie-parser');

const indexRouter = require('./routes/index');

// Initiate Express
const app = express();

// Express Engine Routing Setup
app.use('/', indexRouter);

// Express Engine Setup
app.use(logger('dev'));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Express Engine Error Handling
app.use((req, res, next) => next(createError(404)));

// Modularize Express Engine
module.exports = app;