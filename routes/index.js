// Express Engine
const express = require('express');
const expressRouter = express.Router();

expressRouter.get('/', (req, res, next) => res.send('Hello World'));

module.exports = expressRouter;